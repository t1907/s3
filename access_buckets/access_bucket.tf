data "aws_region" "region" {}
locals {
  region = data.aws_region.region.name
}

resource "aws_s3_bucket" "access_bucket" {
  bucket = "prasad-s3-access-log-${local.region}"
  acl    = "log-delivery-write"
  versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "aws:kms"
        kms_master_key_id = var.kms_key
      }
    }
  }
}

variable "kms_key" {}


output "bucket" {
  value = aws_s3_bucket.access_bucket.arn
}
