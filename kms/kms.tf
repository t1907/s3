data "aws_region" "region" {}
locals {
  region = data.aws_region.region.name
}

resource "aws_kms_key" "kms" {
  description             = "MyKMS"
  deletion_window_in_days = 10
  policy                  = templatefile("./kms_policy.json", { account_id = var.account_id })
}

resource "aws_kms_alias" "kms_alias" {
  target_key_id = aws_kms_key.kms.key_id
  name          = "alias/my-kms-${local.region}"
}


variable "account_id" {}

output "kms_info" {
  value = { key_id = aws_kms_key.kms.key_id,
    alias = aws_kms_alias.kms_alias.name,
  arn = aws_kms_key.kms.arn }
}