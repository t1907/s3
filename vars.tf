variable "region_east" {
  default = "us-east-1"
}

variable "region_west" {
  default = "us-west-2"
}

locals {
  account_id = data.aws_caller_identity.current_user.account_id
}