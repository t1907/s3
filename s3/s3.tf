data "aws_region" "region" {}
locals {
  region = data.aws_region.region.name
}

resource "aws_s3_bucket" "b" {
  for_each = var.buckets
  bucket   = "${each.value}-${local.region}"
  versioning {
    enabled = true
  }

  logging {
    target_bucket = "prasad-s3-access-log-${local.region}"
    target_prefix = "${var.account_id}/${each.value}-${local.region}"
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "aws:kms"
        kms_master_key_id = var.kms_key
      }
    }
  }
}

resource "aws_s3_bucket_public_access_block" "example" {
  for_each                = { for b in aws_s3_bucket.b : b.bucket => b }
  bucket                  = each.key
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

variable "kms_key" {}
variable "account_id" {}
variable "buckets" {}
