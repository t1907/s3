data "aws_region" "region" {}
locals {
  region  = data.aws_region.region.name
  buckets = toset(["mine-my-tf-test-bucket"])
}

provider "aws" {
  region = var.region_east
  default_tags {
    tags = {
      Environment = "Test"
      Owner       = "TFProviders"
      Project     = "Test"
      Region      = var.region_east
    }
  }
}

provider "aws" {
  region = var.region_west
  alias  = "west"
  default_tags {
    tags = {
      Environment = "Test"
      Owner       = "TFProviders"
      Project     = "Test"
      Region      = var.region_west
    }
  }
}

data "aws_caller_identity" "current_user" {}

module "kms_east" {
  source     = "./kms"
  account_id = local.account_id
}

module "kms_west" {
  source     = "./kms"
  account_id = local.account_id
  providers = {
    aws = aws.west
  }
}

module "access_east" {
  source  = "./access_buckets"
  kms_key = module.kms_east.kms_info["arn"]
}

module "access_west" {
  source  = "./access_buckets"
  kms_key = module.kms_west.kms_info["arn"]
  providers = {
    aws = aws.west
  }
}

module "s3" {
  source     = "./s3"
  kms_key    = module.kms_east.kms_info["arn"]
  account_id = local.account_id
  buckets    = local.buckets
}

module "s3_dr" {
  source     = "./s3"
  kms_key    = module.kms_west.kms_info["arn"]
  account_id = local.account_id
  providers = {
    aws = aws.west
  }
  buckets = local.buckets
}